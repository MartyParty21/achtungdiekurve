Achtung die Kurve
=================
- autor: *Martin Rokyta*

Tento projekt slouží jako semestrální práce předmětu Klientské aplikace v javascriptu, vypracované Martinem Rokytou v letním semestru 2022.

Vytvořená hra běží na gitlab pages [zde](https://martyparty21.gitlab.io/achtungdiekurve/)
Zběná dokumentace je k nalezení [zde](https://martyparty21.gitlab.io/achtungdiekurve/doc/doc.html) 
const playerNames = ['fred', 'greenlee', 'pinkey', 'bluebell', 'willem', 'greydon'];
const forbiddenKeys = ['Control', 'Tab', 'Escape', ' ', 'Alt'];
const pointsToWin = [0, 10, 20, 40, 40, 50] // Kolik bodu je potreba k vyhre, serazeno od hry jednoho hrace (nejde vyhrat) do hry sesti hracu

const gameAudio = new Audio('sources/sounds/adventure.mp3');
const crashAudio = new Audio('sources/sounds/8-bit-bomb-explosion.wav');

let T;

let musicOn = true;
let soundsOn = true;

// Sirka stroku hadu
const strokeWidthArg = 6;
// Polomer hlavy hadu
const headRadiusArg = 4;
// Delka ocasu, ktery se vykresli za hadem na zacatku hry
const startingTailLength = 15;
// Vyska hry - jako viewport svg
const gameHeight = 800;
// Sirka hry - jako viewport svg
const gameWidth = 800;
// Rychlost hry - multiplier prirustku ocasu podle casu ticku
const gameSpeed = 0.1;
// Mozna zmena smeru hadu za jeden tick
const headingChangeMultiplier = 0.05;
// Pocet ticku ktere se vykresluje mezera
const spaceLengthInTicks = 7;

// Zmeni obrazovku mezi hrou a menu
const changeScreen = (changeToGame) => {
  let offloadedScreen = changeToGame ? 'menu-screen' : 'game-screen';
  let onloadedScreen = changeToGame ? 'game-screen' : 'menu-screen';

  document.querySelector(`#${offloadedScreen}`).classList.add('hidden');
  document.querySelector(`#${onloadedScreen}`).classList.remove('hidden');
}

class Player {
  constructor(name) {
    this.name = name;
    this.leftKey = '';
    this.rightKey = '';
    this.ready = false;
    this.headElement = null;
    this.pathElement = null;
    this.posX = 0;
    this.posY = 0;
    this.heading = 0;
    this.score = 0;
    this.alive = false;
    this.spaceTicks = -1; // Pocet ticku ktere zbyvaji pro vyrkesleni mezery. -1 Pokud nema byt mezera
  }

  reset() {
    this.leftKey = '';
    this.rightKey = '';
    this.ready = false;
    this.getNameElement().classList.remove('input-assigned');
  }

  // Vrati element, ktery v menu nastavuje hrace
  getNameElement() {
    return document.querySelector(`#${this.name}-input`);
  }
}

class Menu {
  constructor() {
    this.players = [];
    // Hrac ktery je momentalne upravovan
    this.currentlySelectedPlayer = '';

    this.setKey = null; // Metoda ktera se ma vyuzit pro nastaveni ovladani hrace - moznosti assignLeft a assignRight

    // Registruje vsechny hrace a priradi jim jmena podle horniho pole
    playerNames.forEach(playerName => {
      let p = new Player(playerName);
      this.players.push(p);
    })

    // Zaregistrovat eventlistener na kliknuti pro zobrazena jmena vsech hracu
    this.players.forEach(player => {
      const nameElement = player.getNameElement();
      nameElement.addEventListener('click', (e) => {
        // Nastaveni tohoto hrace jako momentalne upravovaneho
        this.currentlySelectedPlayer = player;
        // Pokud jsme kliknuli na jiz nastaveneho hrace, zresetujeme
        if(this.currentlySelectedPlayer.ready) {
          this.currentlySelectedPlayer.reset();
          this.currentlySelectedPlayer = '';
        }
        this.updateSelected();
      })
    });

    this.updateSelected(); // Nakonci konstruktoru, aby se obrazovka dostala do uvodniho stavu, pokud byla po minule hre jina
  }

  // Zpracovani key eventu pro menu
  handleKeyEvent(e) {
    // Pri escape prestat nastavovani hrace
    if(e.key === 'Escape') {
      if(this.currentlySelectedPlayer != null && !this.currentlySelectedPlayer.ready) {
        this.currentlySelectedPlayer.reset();
      }
      this.currentlySelectedPlayer = '';
      this.updateSelected();
    } else if (!forbiddenKeys.includes(e.key) && this.currentlySelectedPlayer !== '') {
      // pri jakemkoliv jinem, povolenem znaku pro ovladani, zavolat nastaveni klavesy - metoda se meni vramci nastaveni
      if(this.setKey != null) {
        this.setKey(this.currentlySelectedPlayer, e.key);
      }
    }
  }

  // Updatuje vizual nastaveni aby souhlasil s nastavenim
  updateSelected() {
    this.players.forEach(player => {
      // Nastavi klaves
      document.querySelector(`#${player.name}-left`).textContent = player.leftKey;
      document.querySelector(`#${player.name}-right`).textContent = player.rightKey;
      // Pridani tridy ktera udrzi nastaveni rozsvicene, pokud je hrac ready
      if(player.ready) {
        player.getNameElement().classList.add('input-assigned');
      }

      if(player === this.currentlySelectedPlayer) {
        // Pokud se tento hrac prave upravuje, nastavi se mu tria aby svitil
        player.getNameElement().classList.add('input-selected');
        // Nastaveni metody kterou vola posluchac stisknuti klavesy, aby nastavila levo
        this.setKey = this.assignLeft;
        document.querySelector(`#${player.name}-left`).textContent = 'Press any key'
      } else {
        // Pokud neni hrac urpavovany, odstani se mu prislusna trida
        player.getNameElement().classList.remove('input-selected');
      }
    })
  }

  // Priradi klavesu ktera ma ovladat zatoceni vlevo pro hrace
  assignLeft(player, key) {
    player.leftKey = key;
    document.querySelector(`#${player.name}-left`).textContent = key;
    document.querySelector(`#${player.name}-right`).textContent = 'Press any key';
    this.setKey = this.assignRight;
  }


  // Priradi klavesu ktera ma ovladat zatoceni vpravo pro hrace
  assignRight(player, key) {
    player.rightKey = key;
    document.querySelector(`#${player.name}-right`).textContent = key;
    this.setKey = null;
    player.ready = true;
  }

  // Vraci vsechny hrace kteri jsou nastaveni pro hru
  getReadyPlayers() {
    return this.players.filter(player => player.ready);
  }
}

// Trida, ktera obstaravat vykreslovani hry na svg
class GamePainter {
  constructor(players) {
    this.players = players;
    this.svgNamespace = 'http://www.w3.org/2000/svg';
    this.gameboardSvg = document.querySelector('#gameboard');
    this.gameboardSvg.setAttributeNS(null, 'viewBox', `0 0 ${gameWidth} ${gameHeight}`)
  }

  repaint() {
    // Prekresluji se pouze hraci, kteri jsou zivi
    this.players.filter(player => player.alive).forEach(player => {
      // Posunuti hlavy hrace (svg circle) na aktualni pozici
      player.headElement.setAttributeNS(null, 'cx', player.posX);
      player.headElement.setAttributeNS(null, 'cy', player.posY);

      // Updatesvg cesty, ktera zobrazuje ocas
      let d = player.pathElement.getAttributeNS(null, 'd');
      let pathCommand = player.spaceTicks === -1 ? 'L' : 'M'; // Pokud prave hrac vykresluje mezeru, pouze posune ukazatel, misto kresleni
      // Vykreslit drahu lehce za hlavu, aby se nenahlasila kolize sama se sebou
      let pathX = player.posX - Math.sin(player.heading) * headRadiusArg / 2;
      let pathY = player.posY - Math.cos(player.heading) * headRadiusArg / 2;
      d += `${pathCommand} ${pathX} ${pathY} `;
      player.pathElement.setAttributeNS(null, 'd', d);
    })
  }

  // Obnovi cele vykresleni pro dalsi kolo, nebo na zacatku cele hry
  resetSvg() {
    // Skryje oznameni viteze, pokud zustalo po minule hre
    document.querySelector('#winner-announcement').classList.add('hidden');
    this.gameboardSvg.innerHTML = ""; // Vymazani svg
    // Prvotni/opetovne nastaveni vykreslovani hrace
    this.players.forEach(player => this.resetPlayerSvg(player));
  }

  // Zobrazi ohlaseni viteze
  showWinner(winner) {
    const announcementElement =document.querySelector(`#winner-announcement`);
    announcementElement.innerHTML = `${winner.name} won!`
    announcementElement.classList.add(winner.name); // Obarvi text barvou hrace
    announcementElement.classList.remove('hidden');
  }

  resetPlayerSvg(player) {
    // Prepsani cesty
    player.pathElement = document.createElementNS(this.svgNamespace, 'path');
    player.pathElement.classList.add(player.name); // Pridani tridy se jmenem hrace urci barvu tahu
    // Nastaveni parametru tahu svg cesty tela hrace
    player.pathElement.setAttributeNS(null, 'stroke-width', `${strokeWidthArg}px`);
    player.pathElement.setAttributeNS(null, 'stroke-linejoin', 'round');
    player.pathElement.setAttributeNS(null, 'fill', 'transparent');
    player.pathElement.setAttributeNS(null, 'pointer-events', 'visibleStroke');

    // Pro vykresleni pocatecniho ocasku hrace je potreba vypocist jak daleko zacit kreslit, a skoncit tesne pred stredem budouci hlavy
    // aby se na zacatku nehlasila kolize sama se sebou
    const pathStartX = player.posX - Math.sin(player.heading) * startingTailLength;
    const pathStartY = player.posY - Math.cos(player.heading) * startingTailLength;
    let pathX = player.posX - Math.sin(player.heading) * headRadiusArg / 2;
    let pathY = player.posY - Math.cos(player.heading) * headRadiusArg / 2;

    let d = `M ${pathStartX} ${pathStartY} L ${pathX} ${pathY} `;
    player.pathElement.setAttributeNS(null, 'd', d);
    this.gameboardSvg.appendChild(player.pathElement);

    // Vytvoreni elementu hlavy hrace
    player.headElement = document.createElementNS(this.svgNamespace, 'circle');
    player.headElement.setAttributeNS(null, 'cx', player.posX);
    player.headElement.setAttributeNS(null, 'cy', player.posY);
    player.headElement.setAttributeNS(null, 'r', `${headRadiusArg}`);
    this.gameboardSvg.appendChild(player.headElement);
  }
}

// Trida obsluhujici celou hru
class Game {
  constructor(players) {
    this.paused = true;
    this.players = players;
    this.keyStates = {}; // Pole ktere udrzuje stav klaves ktere slouzi k ovladani
    this.roundEnded = false;
    this.gameEnded = false;
    this.pointsToVictory = pointsToWin[this.players.length - 1]
    this.players.forEach(player => player.score = 0);

    document.querySelector('#goal-points').innerHTML = `${this.pointsToVictory}`;

    this.updateScoreboard();
    this.painter= new GamePainter(players);
    this.resetRound(); // Inicializuje hru
  }

  // metoda ktera obsluhuje key eventy pro hru
  // updatuje pole booleanu, kde true znamena stav stisknute klavesy, na index urceny (ne)stisknutou klavesou
  handleKeyEvent(e) {
    if(e.type === 'keydown') {
      this.keyStates[e.key] = true;
    }
    if(e.type === 'keyup') {
      this.keyStates[e.key] = false;
    }
  }

  // Upravi scoreboard
  updateScoreboard() {
    let scoreboardEl = document.querySelector('#scoreboard-points');
    scoreboardEl.innerHTML = '';
    // Seradi hrace podle skore
    this.players.sort((a, b) => b.score - a.score).forEach(player => {
      let playerScoreRow = document.createElement('div');
      let playerNameEL = document.createElement('span');
      let scoreEL = document.createElement('span');
      playerNameEL.textContent = player.name;
      scoreEL.textContent = player.score;

      playerScoreRow.append(playerNameEL, scoreEL);

      playerScoreRow.classList.add(player.name, 'points-row'); // Toto prida barvu
      scoreboardEl.append(playerScoreRow);
    })
  }

  // Zacne/pokracuje hru
  start() {
    if(this.gameEnded) return;
    if(this.roundEnded) {
      this.resetRound();
    } else {
      this.paused = false;
      if(musicOn)
        gameAudio.play();
      requestAnimationFrame(this.tick.bind(this));
    }
  }

  // Zastavi hru
  pause() {
    T = null;
    this.paused = true;
    gameAudio.pause();
  }

  // Spusti nove kolo
  resetRound() {
    T = null;
    gameAudio.currentTime = 0;
    this.roundEnded = false;
    this.paused = true;
    this.players.forEach(player => {
      // Nahodne nastaveni pozic a smeru hracu - alespon 10 % velikosti herni plochy od kraju
      player.posX = gameWidth * 0.1 + Math.random() * gameWidth * 0.8;
      player.posY = gameHeight * 0.1 + Math.random() * gameHeight * 0.8;
      player.heading = Math.random() * Math.PI * 2;
      player.alive = true;
    });
    this.painter.resetSvg();
  }

  tick(timestamp) {
    if(this.paused) return;

    if(!T) {
      T = timestamp;
    }
    let dt = timestamp - T;
    T = timestamp;

    if(musicOn && gameAudio.paused) {
      gameAudio.play();
    }
    if(!musicOn && !gameAudio.paused) {
      gameAudio.pause();
    }

    // Updatuje kazdeho ziveho hrace
    this.players.filter(player => player.alive).forEach(player => this.updatePlayer(player, dt))
    this.painter.repaint();
    this.updateScoreboard();
    requestAnimationFrame(this.tick.bind(this));
  }

  // Trida ktera obsluhuje kolize
  handleCollisions(player) {
    // Pokud hrac vyjel z herniho pole, konci
    if(player.posX < 0 || player.posX > gameWidth || player.posY < 0 || player.posY > gameHeight) {
      this.killPlayer(player);
      return;
    }
    // Ziska boundingbox svg elementu hlavy hrace, aby mohl detekovat kolize podle pozice v dokumentu
    const playerHeadBoundingBox = player.headElement.getBoundingClientRect();
    // Ziska absolutni pozici hrace v dokumentu
    const playerAbsoluteX = playerHeadBoundingBox.x + playerHeadBoundingBox.width / 2;
    const playerAbsoluteY = playerHeadBoundingBox.y + playerHeadBoundingBox.height / 2;

    // Ziska seznam vsech DOM elementu, ktere jsou na stejne pozici jako hlava hrace
    const elementsOnHeadPos = document.elementsFromPoint(playerAbsoluteX, playerAbsoluteY);
    // Pokud je na pozici hrace svg element cesty jakehokoliv jineho hrace, hrac konci
    this.players.forEach(differentPlayer => {
      if(elementsOnHeadPos.includes(differentPlayer.pathElement)) {
        this.killPlayer(player);
      }
    });
  }

  // Metoda ktera ukonci hrace
  killPlayer(player) {
    crashAudio.currentTime = 0;
    if(soundsOn)
      crashAudio.play();
    player.alive = false;
    // Pridani vsem zivym hracum po jednom bodu
    this.addPointToAlivePlayers();
    // Pokud uz zije jen jeden hrac, ukonci hru
    if(this.players.filter(player => player.alive).length <= 1) {
      this.pause();
      this.roundEnded = true;
      document.querySelector('#space-info-action').textContent = 'continue';
      this.checkWinningConditions();
    }
  }

  // Metoda ktera vsem zivym hracum prida bod
  addPointToAlivePlayers() {
    this.players.filter(player => player.alive).forEach(player => player.score++);
  }

  // Zkontroluje jestli jsou naplnene podminky konce hry - nektery z hracu ziskal body potrebne k vitezstvi, a ma alespon dva body rozdil
  checkWinningConditions() {
    // Pokud je pouze jeden hrac, ukonci vzdy
    if(this.players.length === 1) {
      this.gameEnded = true;
      this.painter.showWinner(this.players[0]);
      return;
    }
    // Seradi hrace podle bodu
    const sortedPlayers = this.players.sort(player => player.score);
    let possibleWinner = sortedPlayers[0];
    let possibleRunnerUp = sortedPlayers[1];
    // Pokud ma vitez dostatek bodu k vyhre, a alespon dvoubodovy naskok nad druhym, ukonci hru
    if(possibleWinner.score >= this.pointsToVictory && possibleWinner.score - possibleRunnerUp.score > 1) {
      this.gameEnded = true;
      this.painter.showWinner(possibleWinner);
    }
  }

  updatePlayer(player, dt) {
    if(dt === 0) {
      dt = 1;
    }
    // Pokud jsou zaznamenane stisknute prislusne klavesy, zmeni smer hrace prictenim konstanty
    if(this.keyStates[player.leftKey]) {
      player.heading += headingChangeMultiplier;
    }
    if(this.keyStates[player.rightKey]) {
      player.heading -= headingChangeMultiplier;
    }
    // Updatuje pozici
    player.posX += Math.sin(player.heading) * gameSpeed * dt;
    player.posY += Math.cos(player.heading) * gameSpeed * dt;
    if(player.spaceTicks === -1) {
      // Pokud hrac neni ve stavu "mezera", tedy pokud normalne vykresluje trasu, s pravdepodobnisti 0.5% se zacne mezera
      let chance = Math.random();
      if (chance > 0.995) {
        // Zacit mezeru
        player.spaceTicks = spaceLengthInTicks;
      } else {
        // Pokud se nezacala mezera, obslouzi mozne kolize
        this.handleCollisions(player);
      }
    } else { // Pokud je hrac ve stavu "mezera"
      // Pokud prave konci mezera, nastav pocitadlo na -1 a skonci
      if(player.spaceTicks === 0) {
        player.spaceTicks = -1;
        return;
      }
      // Pokud je hrac ve stavu mezera, muze projizdet zdmi. Pokud vyjede z herniho planu, zobrazi se na druhe strane
      if(player.posX < 0) {
        player.posX = gameWidth;
      } else if (player.posX > gameWidth) {
        player.posX = 0;
      }
      if(player.posY < 0) {
        player.posY = gameHeight;
      } else if(player.posY > gameHeight) {
        player.posY = 0;
      }
      // Odecist tick kdy jse hrac ve stavu mezera
      player.spaceTicks--;
    }
  }
}

// Hlavni trida, obsluhujcici celou hru
class AchtungKurve {
  constructor() {
    this.ingame = false;
    this.game = null;
    this.menu = new Menu();
    this.spaceAction = this.startGame; // Udrzuje metodu ktera se ma volat pri stisknuti mezerniku - bud startGame nebo toggleGame

    this.loadSettingFromLocalStorage();
    this.registerListeners();
  }

  // Metoda ktera zacne hru - jedna z metod ktera muze byt volana pri stisku mezerniku.
  startGame() {
    // Ziska hrace ktere jsou nastaveni pro hru
    let players = this.menu.getReadyPlayers();

    // Pokud je pripraven alespon jeden hrac, zacne hru
    if(players.length > 0) {
      this.ingame = true;
      // Zacne hru s pripravenymi hraci
      this.game = new Game(this.menu.getReadyPlayers());
      changeScreen(true); // Zmeni obrazovku na hru
      this.spaceAction = this.toggleGame; // Nastavi metodu pri stisku mezerniku na toggleGame
      document.querySelector('#space-info-action').textContent = 'start';
      this.flashSpaceInfo();
    }
  }

  // Metoda ktera pauzuje/odpauzuje hru - jedna z metod ktera muze byt volana pri stisku mezerniku
  toggleGame() {
    // Pokud hra skoncila, zpatky do menu
    if(this.game.gameEnded) {
      changeScreen(false); // Zmeni obrazovku na menu
      this.spaceAction = this.startGame;
      this.ingame = false;
    } else if(this.game.paused) {
      // Pokud je hra pauznuta, pokracuje
      document.querySelector('#space-info-action').textContent = 'pause';
      this.flashSpaceInfo();
      this.game.start()
    } else {
      // Pokud hra bezi, pauzne se
      document.querySelector('#space-info-action').textContent = 'continue';
      this.flashSpaceInfo();
      this.game.pause();
    }
  }

  flashSpaceInfo() {
    const el = document.querySelector('#space-info');
    el.classList.add('pulse');
    setTimeout( () => {
        el.classList.remove('pulse');
      }, 500
    )
  }

  registerListeners() {
    // Pridani posluchace na stisk klaves. Vola odpovidajici metodu, ktera stisk obslouzi, budto pro menu nebo pro hru
    document.addEventListener('keydown', (e) => {
      // Pro mezernik vzdy zavolat spaceAction
      if(e.code === 'Space') {
        document.querySelector('#menu-start-info').classList.add('space-active');
        this.spaceAction();
      } else if (this.ingame) {
        this.game.handleKeyEvent(e); // Pokud jsme ve hre, vola se handleKeyEvent hry
      } else {
        this.menu.handleKeyEvent(e); // Pokud nejsme ve hre, vola se handleKeyEvent menu
      }
    });

    // Naveseni posluchace na zvednuti klavesy - to vyuziva jen hra, ktera tim ziska informaci ze jiz nema pripadny hrat zatacet, a menu pro zmenseni infopanelu
    document.addEventListener('keyup', (e) => {
      // Pokud se uvolnil mezernik, zmensi start-info div
      if(e.code === 'Space') {
        document.querySelector('#menu-start-info').classList.remove('space-active');
      }
      if(this.game != null) {
        this.game.handleKeyEvent(e);
      }
    });
    // Naveseni posluchace na navrat do menu
    document.querySelector(`#backToMenu-button`).addEventListener('click', e => {
      changeScreen(false);
      this.game.pause();
      this.spaceAction = this.startGame;
      this.ingame = false;
    });
    // Naveseni posluchace na hudebni checkbox
    document.querySelector('#music-checkbox').addEventListener('change', e => {
      musicOn = e.currentTarget.checked;
      localStorage.setItem('music-setting', musicOn)
    })
    // Naveseni posluchace na zvykovy checkbox
    document.querySelector('#sound-checkbox').addEventListener('change', e => {
      soundsOn = e.currentTarget.checked;
      localStorage.setItem('sound-setting', soundsOn);
    })
  }

  loadSettingFromLocalStorage() {
    const musicCheckbox = document.querySelector('#music-checkbox');
    const soundCheckbox = document.querySelector('#sound-checkbox');

    const musicVal = localStorage.getItem('music-setting');
    if(musicVal != null && musicVal === 'false') {
      musicOn = false;
      musicCheckbox.checked = false;
    } else {
      musicOn = true;
      musicCheckbox.checked = true;
    }

    const soundsVal = localStorage.getItem('sound-setting');
    if(soundsVal != null && soundsVal === 'false') {
      soundsOn = false;
      soundCheckbox.checked = false;
    } else {
      soundsOn = true;
      soundCheckbox.checked = true;
    }
  }
}

// Vytvoreni hry
new AchtungKurve();